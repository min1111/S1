package com.cos.testspring.repository;

import com.cos.testspring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User,Integer> {

//    @Modifying
//    @Query(value="INSERT INTO user(username,password) VALUES (:username,:password);",nativeQuery=true)
//    void mSave(@Param("username") String username, @Param("password") String password);


//    @Modifying
//    @Query(value="SELECT email FROM user WHERE (:email);",nativeQuery=true)
//    User mFind(@Param("email") String email);

    public User findByEmail(String Email);
    public User findByUsername(String username);

}
