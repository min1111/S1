package com.cos.testspring.repository;

import com.cos.testspring.model.Board;
import com.cos.testspring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BoardRepository extends JpaRepository<Board,Integer> {

//    @Modifying
//    @Query(value="INSERT INTO user(username,password) VALUES (:username,:password);",nativeQuery=true)
//    void mSave(@Param("username") String username, @Param("password") String password);

//    public User findByUsername(String Username);

    @Query(value="select * from board",nativeQuery=true)
    List<Board> mRecent();

}
