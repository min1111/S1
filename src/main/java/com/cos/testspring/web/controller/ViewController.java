package com.cos.testspring.web.controller;

import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.model.Board;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.service.BoardService;
import com.cos.testspring.web.dto.BoardDto;
import com.cos.testspring.web.dto.CMRespDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class ViewController {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final BoardService boardService;


    /*==========인덱스==========*/
    @GetMapping("/test")
    public String index(){

        return "temp";

    }


}
