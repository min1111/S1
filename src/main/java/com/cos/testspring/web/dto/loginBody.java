package com.cos.testspring.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@Data
public class loginBody {
    private String role;
	private String email;
    private Boolean login;

    public loginBody(String role,String email,Boolean login){
        super();
        this.role = role;
        this.email = email;
        this.login = login;
    }
}
