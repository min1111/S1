package com.cos.testspring.web.dto;

import com.cos.testspring.model.Board;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BoardDto {

	//private MultipartFile file; // 파일은 MultipartFile을 통해 받을 수 있음
	private String title;
	private String content;


	public Board toEntity() {
		return Board.builder()
				.title(title)
				.content(content)
				.build();
	}
}
