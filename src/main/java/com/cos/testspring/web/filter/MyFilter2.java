package com.cos.testspring.web.filter;


import javax.servlet.*;
import java.io.IOException;

public class MyFilter2 implements Filter { // Filter를 implements해주면 필터가 된다.


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        System.out.println("필터22");
        chain.doFilter(request,response); // 이 프로세스를 계속 진행하려면 chain에다가 필터를 이렇게 넘겨주어야함.

    }
}
