package com.cos.testspring.web.api;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.model.User;

@RestController
public class SecurityController {
    
    @GetMapping("/createToken/{username}/{jwtToken}")
    public ResponseEntity<?> createToken(HttpServletResponse response,@PathVariable String username,@PathVariable String jwtToken){
        
        System.out.println("토큰생성");

        System.out.println(username);
        System.out.println(jwtToken);

        Cookie myCookie = new Cookie("accessToken", jwtToken);
        myCookie.setMaxAge(60000*10);
        myCookie.setPath("/"); // 모든 경로에서 접근 가능 하도록 설정
        response.addCookie(myCookie);

        return new ResponseEntity<>((null),HttpStatus.OK);
    }

}