package com.cos.testspring.web.api;

import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.model.Board;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.service.BoardService;
import com.cos.testspring.web.dto.BoardDto;
import com.cos.testspring.web.dto.CMRespDto;
import com.cos.testspring.web.dto.loginBody;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RequiredArgsConstructor
@RestController
public class ApiController {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final BoardService boardService;


    /*==========인덱스==========*/
//    @GetMapping("/")
//    public ResponseEntity<?> index(@AuthenticationPrincipal PrincipalDetails principalDetails){
//
//        return new ResponseEntity<>(new CMRespDto<>(1,"로그인성공",principalDetails.getUser().getEmail(),null),HttpStatus.OK);
//
//    }



    /*==========회원가입==========*/
    //@Secured("ROLE_USER")
    @PostMapping("/join")
    public @ResponseBody ResponseEntity<?> join(User user){
        System.out.println("----------");
        System.out.println(user);
        System.out.println(user.getEmail());
        System.out.println("----------");
        if(userRepository.findByEmail(user.getEmail()) != null){
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            return new ResponseEntity<>(new CMRespDto<>(-1,"이미 존재하는 계정입니다.",null,null),HttpStatus.OK);
        }

        user.setRole("ROLE_USER");

        String rawPassword = user.getPassword();
        String encPassword = bCryptPasswordEncoder.encode(rawPassword);

        user.setUsername(user.getUsername());
        user.setPassword(encPassword);

        userRepository.save(user);
        return new ResponseEntity<>(new CMRespDto<>(1,"회원가입성공",null,null),HttpStatus.OK);
    }

    @PostMapping("/templogin")
    public @ResponseBody ResponseEntity<?> templogin(User user){

        System.out.println("여기"+user);
        System.out.println(user.getEmail());
        System.out.println(user.getPassword());

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        User userEntity = userRepository.findByEmail(user.getEmail());

        if(userEntity!=null){ // DB에 있다면

            if(encoder.matches(user.getPassword(),userEntity.getPassword())){
                return new ResponseEntity<>(new CMRespDto<>(1,"로그인성공",userEntity.getEmail(),null),HttpStatus.OK);
            }else{
                return new ResponseEntity<>(new CMRespDto<>(-1,"비밀번호가 다릅니다.",null,null),HttpStatus.OK);
            }

        }

       return new ResponseEntity<>(new CMRespDto<>(-1,"로그인실패",null,null),HttpStatus.OK);

    }


    /*==========글쓰기==========*/
    @PostMapping("/api/v1/user/posting")
    public @ResponseBody ResponseEntity<?> posting(BoardDto boardDto){

        boardService.insertPosting(boardDto); // DB 저장

        return new ResponseEntity<>(new CMRespDto<>(1,"글 포스팅 성공",boardDto,null),HttpStatus.OK);

    }

    /*==========게시판 불러오기============*/
    @GetMapping("/api/v1/user/board")
    public ResponseEntity<?> getBoard(){

        Board tempboard = new Board();
        tempboard.setTitle("테스트제목");
        tempboard.setContent("테스트내용입니다.");

        List<Board> board = boardService.getBoard();
        board.add(tempboard);
        System.out.println("게시판");
        return new ResponseEntity<>(new CMRespDto<>(1,"게시판 불러오기 성공",null,board),HttpStatus.OK);
    }


    @GetMapping("/hello/{jwtToken}")
    public ResponseEntity<?> hello(@PathVariable String jwtToken){

        return new ResponseEntity<>(jwtToken,HttpStatus.OK);

    }
    @GetMapping("/message/{jwtToken}/{email}/{role}/{login}")
    public ResponseEntity<?> messageForHeader(HttpServletRequest request, HttpServletResponse response,@PathVariable String jwtToken,@PathVariable String email,@PathVariable String role,@PathVariable Boolean login) {
        response.addHeader("Authorization", "Bearer"+jwtToken);
        loginBody loginBody = new loginBody(role,email,login);
        if(login){
        return new ResponseEntity<>(new CMRespDto<>(1,"로그인 성공",loginBody,null),HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new CMRespDto<>(-1,"로그인 실패",loginBody,null),HttpStatus.OK);
        }
    }

    @PostMapping("/token")
    public String token(){
        return "<h1>home</h1>";
    }

    @PostMapping("/tempjoin")
    public String tempjoin(@RequestBody User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRole("ROLE_USER");
        userRepository.save(user);
        return "회원가입완료";
    }

    //user,manager,admin 권한 접근가능
    @GetMapping("/api/v1/user")
    public String user(Authentication authentication) {
        PrincipalDetails principal = (PrincipalDetails) authentication.getPrincipal();
        System.out.println("principal : "+principal.getUser().getId());
        System.out.println("principal : "+principal.getUser().getUsername());
        System.out.println("principal : "+principal.getUser().getPassword());

        return "확인";
    }
    @GetMapping("/api/v1/user/check")
    public ResponseEntity<?> userCheck(Authentication authentication) {
        PrincipalDetails principal = (PrincipalDetails) authentication.getPrincipal();
        principal.getUser().setPassword(null);
        return new ResponseEntity<>(new CMRespDto<>(1,"유저확인",principal.getUser(),null),HttpStatus.OK);
    }

    //manager,admin 권한 접근가능
    @GetMapping("/api/v1/manager")
    public String manager(){
        return "manager";
    }

    //admin 권한 접근가능
    @GetMapping("/api/v1/admin")
    public String admin(){
        return "admin";
    }
    @GetMapping("/exception")
    public ResponseEntity<?> exception(){
        loginBody loginBody = new loginBody(null,null,false);
        System.out.println("컨트롤러 도착");
        return new ResponseEntity<>(new CMRespDto<>(-1,"인증실패",loginBody,null),HttpStatus.OK);
    }
    @GetMapping("/successfulLogin")
    public ResponseEntity<?> successfulLogin(){

        return new ResponseEntity<>(new CMRespDto<>(1,"로그인 성공",null,null),HttpStatus.OK);

    }


}
