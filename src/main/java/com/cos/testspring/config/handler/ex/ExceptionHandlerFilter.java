package com.cos.testspring.config.handler.ex;

import com.cos.testspring.web.dto.CMRespDto;
import com.cos.testspring.web.dto.loginBody;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExceptionHandlerFilter extends OncePerRequestFilter {
    @Override//jwt토큰 인증 필터에서 ServletException, IOException 예외가 발생하면
    //setErrorResponse를 통해 오류 메세지를 설정한다.
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException {
        try{
            filterChain.doFilter(request, response);
        }catch (Exception e){
            //토큰의 유효기간 만료
            setErrorResponse(response);
        }
    }
    private void setErrorResponse(
            HttpServletResponse response

    ) {
        ObjectMapper objectMapper = new ObjectMapper();
        response.setStatus(500);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        try {
            response.sendRedirect("/exception");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

