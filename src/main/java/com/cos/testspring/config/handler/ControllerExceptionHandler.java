package com.cos.testspring.config.handler;

import com.cos.testspring.config.handler.ex.CustomApiException;
import com.cos.testspring.web.dto.CMRespDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
@ControllerAdvice
public class ControllerExceptionHandler {
	
	@ExceptionHandler(CustomApiException.class) // RuntimeException을 발생하는 모든 exception을 이 함수가 가로챔
	public ResponseEntity<?> apiException(CustomApiException e) {

		return null;
	}
}
 