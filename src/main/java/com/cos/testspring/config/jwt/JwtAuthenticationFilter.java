package com.cos.testspring.config.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.config.handler.ex.CustomApiException;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.web.dto.CMRespDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StreamUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Date;

//스프링 시큐리티에서 UsernamePasswordAuthenticationFilter 가 있음
// /login 요청해서 username, password 전송하면 (POST)
// UsernamePasswordAuthenticationFilter 필터가 동작함
//근데 formLogin을 disable해놔서 아직 동작안함 => 이 필터만 시큐리티 config에 등록해주면됨
//AuthenticationManager를 통해서 로그인을 진행해서 파람으로 받아야함
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final AuthenticationManager authenticationManager;

    private UserRepository userRepository;
    private static final String KEY = "cos";

    // /login요청을 하면 로그인 시도를 위해서 실행되는 함수
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        System.out.println("JwtAuthenticationFilter:로그인 시도중");
        // 1. username ,password 받아서
        ObjectMapper om = new ObjectMapper(); // json 데이터를 파싱해서 객체에 담아줌. (1)
        User user = null;
        try {
            String messageBody = StreamUtils.copyToString(request.getInputStream(), StandardCharsets.UTF_8);
            
            System.out.println("여기?"+messageBody);

            user = om.readValue(messageBody, User.class);  // json 데이터를 파싱해서 객체에 담아줌. (2)

            System.out.println(user);

            //토큰을 만들어주자
            //이메일로 인증하는 방식을 사용하여 user.getEmail로 변경하여 사용
            UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken(user.getEmail(),user.getPassword());
        
            //이게 실행될 때 PrincipalDetailsService의 loadUserByUsername()함수가 실행됨
            //authenticationManager에 토큰을 넣어던지면 인증을 해준다.
            //authentication에는 내가 로그인한 정보가 담기게 된다.
            Authentication authentication = authenticationManager.authenticate(authenticationToken);

            // authentication객체가 session영역에 저장됨
            PrincipalDetails principalDetails = (PrincipalDetails)authentication.getPrincipal(); // 다운캐스팅
            System.out.println(principalDetails.getUser().getEmail()); // 얘가 출력이 된다는건 => 로그인이 되었다는 뜻

            // authentication 객체가 session영역에 저장을 해야하고 그 방법이 return 해주면됨.
            // 리턴의 이유는 권한 관리를 security가 대신 해주기 때문에 편하려고 하는거임.
            // 굳이 JWT 토큰을 사용하면서 세션을 만들 이유가 없음. 근데 단지 권한 처리 때문에 session을 넣어줍니다.ㄴ
            return authentication;
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("========================================");
        //요류가 발생하여 정상적으로 작동하지 못하면 /message/null/null/null/false경로의 컨트롤러로 이동하여 정해진 값을 리턴한다
        try {
            response.sendRedirect("/message/null/null/null/false");

        } catch (IOException e) {
            e.printStackTrace();
        }
        // 2. 정상인지 로그인 시도를 해봄. authenticationManager로 로그인 시도를 하면 PrincipalDetailsService가 호출됨
        // PrincipalDetailsService가 호출 loadUserByUsername() 함수 실행됨.
        // 3. PrincipalDetails를 세션에 담고 ( 굳이 세션에 담는 이유는 얘를 세션에 안담으면 antMatchers쪽 권한관리가 안됨 )
        // 4. JWT토큰을 만들어서 응답해주면됨
        return null;
    }

    // attemptAuthentication 실행 후 인증이 정상적으로 되었으면 successfulAtentication 함수가 실행됨
    // JWT 토큰을 만들어서 request요청한 사용자에게 JWT토큰을 repose해주면됨
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                                      Authentication authResult) throws IOException, ServletException {
        PrincipalDetails principalDetailis = (PrincipalDetails) authResult.getPrincipal();
        // RSA방식은 아니구 Hash암호방식
        String AjwtToken = JWT.create()
                .withSubject(principalDetailis.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+(60000*10))) //토큰만료시간
                .withClaim("id", principalDetailis.getUser().getId())
                .withClaim("username", principalDetailis.getUser().getEmail())
                .sign(Algorithm.HMAC512(KEY.getBytes(StandardCharsets.UTF_8)));
        String RjwtToken = JWT.create()
                .withSubject(principalDetailis.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+(60000*10))) //토큰만료시간
                .withClaim("id", principalDetailis.getUser().getId())
                .withClaim("username", principalDetailis.getUser().getEmail())
                .sign(Algorithm.HMAC512(KEY.getBytes(StandardCharsets.UTF_8)));
//        User user=  userRepository.findByEmail(principalDetailis.getUser().getEmail());
//        user.setRtoken("Bearer"+RjwtToken);
//        user.setAtoken("Bearer"+AjwtToken);
//        userRepository.save(user);
        //정해진 값을 헤더와 바디에 넘겨주기위해 컨트롤러에 그 값을 넘겨준다.
        response.sendRedirect("/message/"+AjwtToken+"/"+principalDetailis.getUser().getEmail()+
                "/"+principalDetailis.getUser().getRole()+"/true");
    }
}
