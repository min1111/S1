package com.cos.testspring.config.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

// 시큐리티가 filter가지고 있는데 그 필터중에 BasicAuthenticationFilter라는 것이 있음.
// 권한이나 인증이 필요한 특정 주소를 요청했을 때 위 필터를 무조건 타게 되어있음.
// 만약에 권한이나 인증이 필요한 주소가 아니라면 이 필터를 안타요
public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private UserRepository userRepository;
    private static final String KEY = "cos";

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,UserRepository userRepository){
        super(authenticationManager);
        System.out.println("인증이나 권한이 필요한 주소로 요청됨");
        this.userRepository = userRepository;
        }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
       // super.doFilterInternal(request,response,chain);
        System.out.println("인증이나 권한이 필요한 주소 요청이 됨11");

        String jwtHeader = request.getHeader("Authorization");
        System.out.println("jwtHeader:"+jwtHeader);
//        Cookie[] RjwtHeader= request.getCookies();
//        Cookie Rjwt= null;
//        for(Cookie cookie :RjwtHeader){
//            if(cookie.getName().equals("R")){
//                Rjwt=cookie;
//            }
//        }
//        if(Rjwt == null || !Rjwt.getValue().startsWith("Bearer")){ // header에 토큰이 없거나 & Bearer로 시작하지 않으면
//            chain.doFilter(request,response);
//            return;
//        }
        //JWT 토큰을 검증을 해서 정상적인 사용자인지 확인
        if(jwtHeader == null || !jwtHeader.startsWith("Bearer")){ // header에 토큰이 없거나 & Bearer로 시작하지 않으면
            chain.doFilter(request,response);
            return;
        }
        //JWT토큰을 검증을 해서 정상적인 사용자인지 확인
        String jwtToken = request.getHeader("Authorization").replace("Bearer","");

        String username = "";
        try {//아이디를 찾지 못하거나 만료된 토큰이면 리턴하여 ExceptionHandlerFilter로 보낸다
            username = JWT.require(Algorithm.HMAC512(KEY.getBytes(StandardCharsets.UTF_8))).build()
                    .verify(jwtToken).getClaim("username").asString();
        }catch (Exception e){
            chain.doFilter(request,response);
            return;
        }

        System.out.println("유저네임"+username);
        if(username != null && username != ""){ // username이 null이 아니라는건 서명이 정상적으로 됬다는 뜻
            User userEntity = userRepository.findByEmail(username);
            PrincipalDetails principalDetails = new PrincipalDetails(userEntity);
            // Jwt 토큰 서명을 통해서 서명이 정상이면 Authentication 객체를 만들어준다.
            Authentication authentication = new UsernamePasswordAuthenticationToken(principalDetails
                    ,null,principalDetails.getAuthorities());
            // 강제로 시큐리티의 세션에 접근하여 값 저장
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
        chain.doFilter(request,response);
    }
    }