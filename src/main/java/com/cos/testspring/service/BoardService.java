package com.cos.testspring.service;

import com.cos.testspring.model.Board;
import com.cos.testspring.repository.BoardRepository;
import com.cos.testspring.web.dto.BoardDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class BoardService {

    private final BoardRepository boardRepository;

    /*=========글 작성 INSERT=========*/
    public void insertPosting(BoardDto boardDto){

        boardDto.setTitle(boardDto.getTitle());
        boardDto.setContent(boardDto.getContent());

        Board board = boardDto.toEntity();
        Board boardEntity = boardRepository.save(board);
    }

    /*==========게시판 불러오기===============*/
    public List<Board> getBoard(){

        List<Board> board = boardRepository.mRecent();

        return board;
    }



}
